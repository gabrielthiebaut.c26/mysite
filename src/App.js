import './App.css';
import './cards/cards.css'
import {Cards} from './cards/Cards';
import bot from './imgs/bot.jpg';
import google from './imgs/google.jpg';
import cascade from './imgs/cascade.jpg';
import vacation from './imgs/vacation.jpg';
import wallpaper from './imgs/wallpaper.jpg';

function App() {
  return (
    <div className="App">
        <header>
          <h1>MEMENTOR</h1>
        </header>
        <div className ="cards_list">
          <Cards numero={1} image={bot}/>
          <Cards numero={2} image={google}/>
          <Cards numero={3} image={cascade}/>
          <Cards numero={4} image={vacation}/>
          <Cards numero={5} image={wallpaper}/>

          <Cards numero={3} image={cascade}/>
          <Cards numero={4} image={vacation}/>
          <Cards numero={2} image={google}/>
          <Cards numero={5} image={wallpaper}/>
          <Cards numero={1} image={bot}/>
        </div>
    </div>
  );
}

export default App
