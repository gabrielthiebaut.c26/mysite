import React, {useState} from 'react';


export function Cards (props) {

    const [switcher, setSwitch] = useState ('inactive');

    function handlerSwitch(e) {
        if (switcher === 'active') {
            setSwitch ('inactive');
        }
        else {
            setSwitch('active');
        }
    }

    return (
<div className={`Cards ${switcher}`} onClick={handlerSwitch}>
    <div>
        <div class="recto"style={{backgroundImage: `url(${props.image})`}}></div>
        <div class="verso"></div>
    </div>
</div>
    );
}